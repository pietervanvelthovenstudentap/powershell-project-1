﻿function New-CustomChannel{
    [cmdletbinding()]
    param(
    [Parameter(Mandatory,ValueFromPipeline)]
    [string] $GroupId
    )
    process {    
    Connect-MicrosoftTeams ; Import-CSV -path "C:\School\OS Scripting DSN\powershell\newchannels.csv" | ForEach-Object {$ChannelNameOld = $_.channelname ;[PSCustomObject]@{ChannelName = "$ChannelNameOld Pieter Van Velthoven"; GroupId= $GroupId}} | Write-Output | ForEach-Object{New-TeamChannel -GroupId $_.GroupId -DisplayName $_.ChannelName}
    }
}

#TO RUN:
#import-module MSTeamsAssignment (-force when editing)
#'fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736' | New-CustomChannel